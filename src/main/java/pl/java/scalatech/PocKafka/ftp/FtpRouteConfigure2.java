package pl.java.scalatech.PocKafka.ftp;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FtpRouteConfigure2 {
    //@Bean
    RouteBuilder ftp2() {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                //from("ftp://localhost?username=przodownik&password=slawek?binary=true&ftpClient.dataTimeout=3000")
                from("ftp://localhost:21/przodownik/outputs?username=przodownik&password=slawek&noop=true")
                .routeId("ftp2")
                .to("file://output3")
                .log("Sent ${headers['CamelFileName']} to FTP server complete.");

            }
        };

    }
}

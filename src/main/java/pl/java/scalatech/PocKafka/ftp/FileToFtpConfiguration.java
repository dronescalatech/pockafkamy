package pl.java.scalatech.PocKafka.ftp;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.annotation.Bean;

//@Configuration
public class FileToFtpConfiguration {
    @Bean
    RouteBuilder file() {
        return new RouteBuilder() {

            @Override
            public void configure() throws Exception {
                from("{{input.file}}?noop=true")
                        .routeId("fileReader")
                        .log("Uploading file ${file:name}")
                        .to("direct:outgoingFiles");
            }
        };

    }
}

package pl.java.scalatech.PocKafka.ftp;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FtpRouteConfigure {
    @Bean
    RouteBuilder ftp() {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("{{input.file}}")
                         .doTry()
                        .routeId("ftp")
                        .log("File name: ${headers['CamelFileName']}")//  content : ${body}
                        // .to("file://outgoing")
                        // {{ln.mc.user}}@{{ln.mc.host}}/{{ln.mc.path}}?password={{ln.mc.password}}
                        //.to("ftp://przodownik@localhost:21?password=slawek&binary=true?passiveMode=true&ignoreFileNotFoundOrPermissionError=true")
                        .to("ftp://localhost:21/przodownik/outputs?username=przodownik&password=slawek&passiveMode=true")
                        .doCatch(Throwable.class)
                        .to("file://error").end()
                        .log("Sent ${headers['CamelFileName']} to FTP server complete.");

            }
        };

    }
}

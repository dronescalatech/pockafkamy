package pl.java.scalatech.PocKafka;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class PocKafkaApplication implements ApplicationRunner {

    /*@Autowired
    private GreetingsService greetingsService;*/

    public static void main(String[] args) {
        SpringApplication.run(PocKafkaApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        /*IntStream.rangeClosed(1, 10)
                 .mapToObj(this::createGreetings)
                 .forEach(g -> greetingsService.sendGreeting(g));*/
    }

    /*private Greetings createGreetings(int i) {
        long time = now().atZone(systemDefault()).toInstant().toEpochMilli();
         Greetings greetings = new Greetings(time, "hello : " + i);
         log.info("send : {}",greetings);  
        return greetings;
    }*/
}
